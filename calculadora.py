#!/usr/bin/python3

import sys
import operator

if len(sys.argv) == 4:
	try:
		function = sys.argv[1]
		num1 = float(sys.argv[2])
		num2 = float(sys.argv[3])
	except ValueError:
		sys.exit("Incorrect format")

	dict= {"suma": operator.add,
		"resta" : operator.sub,
		"multiplicacion" : operator.mul,
		"division" : operator.truediv}

	ans = dict[function](num1,num2)

	print("Result = {}".format(ans))
else:
	sys.exit("<function> <num1> <num2>")
